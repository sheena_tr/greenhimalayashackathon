﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreenHimalayasHackathon.Models
{
    public class TreeViewModel
    {
        public int Tree_id { get; set; }
        public string Carbon_sequetered_per_year { get; set; }
        public string Age { get; set; }
        public Nullable<System.DateTime> Plantation_date { get; set; }
        public Nullable<int> Project_id { get; set; }
        public string Plant_type { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string NgoName { get; set; }
        public string strLat { get; set; }
        public string strLong { get; set; }
    }
}