﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreenHimalayasHackathon.Models
{
    public class ProjectVerificationModel
    {
        public int Project_id { get; set; }
        public string Project_location { get; set; }
        public Nullable<int> No_of_Trees { get; set; }
        public string Inventory { get; set; }
        public string Project_type { get; set; }
        public Nullable<int> Ngo_id { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }

        public string verification_id { get; set; }
        public string project_name { get; set; }
        public string Status { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.DateTime> Verification_date { get; set; }

    }
}