﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GreenHimalayasHackathon;
using GreenHimalayasHackathon.Models;

namespace GreenHimalayasHackathon.Controllers
{
    public class NewProjectController : Controller
    {
        private GreenHimalayas_WebEntities db = new GreenHimalayas_WebEntities();

        // GET: NewProject
        public ActionResult Index()
        {
            int id = Convert.ToInt32(Session["NgoID"]);
            if (id == null)
            {
                return RedirectToAction("Login", "Home");
            }

            //  List<Project_Table> project_Tables = new List<Project_Table>();
            var project_Tables = db.Project_Table.Where(a => a.Ngo_id == id).ToList();
            if (project_Tables == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View(project_Tables);
        }

        public ActionResult ProjectCard()
        {
            int id = Convert.ToInt32(Session["NgoID"]);
            if (id == null)
            {
                return RedirectToAction("Login", "Home");
            }

            //  List<Project_Table> project_Tables = new List<Project_Table>();
            var project_Tables = db.Project_Table.Where(a => a.Ngo_id == id).ToList();
            if (project_Tables == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View(project_Tables);
        }

        // GET: NewProject/Details/5
        public ActionResult Details(int? id)
        {
            int Ngo_id = Convert.ToInt32(Session["NgoID"]);
            if (id == null)
            {
                return RedirectToAction("Login", "Home");
            }

            //  List<Project_Table> project_Tables = new List<Project_Table>();
            List<ProjectVerificationModel> projectVerificationModel = new List<ProjectVerificationModel>();
            var project_Tables = (from a in db.Project_Table
                                  join c in db.Verification_Table on a.Project_id equals c.Project_id 
                                  where a.Project_id == id
                                  select new ProjectVerificationModel()
                                  {
                                    Project_location =  a.Project_location,
                                    Project_type =  a.Project_type,
                                    project_name =  c.project_name,
                                    Status =  c.Status,
                                    Amount = c.Amount,
                                    No_of_Trees = a.No_of_Trees,
                                    Inventory = a.Inventory,
                                    Created_Date = a.Created_Date
                                  }).FirstOrDefault();
          
            // var project_Tables = db.Project_Table.Where(a => a.Project_id == id).FirstOrDefault();

            if (project_Tables == null)
            {
                return RedirectToAction("Index", "NewProject");
            }
            return View(project_Tables);
        }

        // GET: NewProject/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NewProject/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Project_id,Project_Name,Project_location,No_of_Trees,Inventory,Project_type,Ngo_id,Created_Date")] Project_Table project_Table)
        {
            if (ModelState.IsValid)
            {
                project_Table.Ngo_id = Convert.ToInt32(Session["NgoID"]);
                db.Project_Table.Add(project_Table);
                db.SaveChanges();
                int amount = Convert.ToInt32(project_Table.No_of_Trees * 100);
                var Projectid = project_Table.Project_id;
                Verification_Table verification_tbl = new Verification_Table();
                verification_tbl.project_name = project_Table.Project_Name;
                verification_tbl.Status = "Pending";
                verification_tbl.Amount = amount;
                verification_tbl.Project_id = Convert.ToInt32(Projectid);
                verification_tbl.Ngo_id = Convert.ToInt32(Session["NgoID"]);
                db.Verification_Table.Add(verification_tbl);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(project_Table);
        }

        // GET: NewProject/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project_Table project_Table = db.Project_Table.Find(id);
            if (project_Table == null)
            {
                return HttpNotFound();
            }
            return View(project_Table);
        }

        // POST: NewProject/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Project_id,Project_Name,Project_location,No_of_Trees,Inventory,Project_type,Ngo_id,Created_Date")] Project_Table project_Table)
        {
            if (ModelState.IsValid)
            {
                project_Table.Ngo_id = Convert.ToInt32(Session["NgoID"]);
                db.Entry(project_Table).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(project_Table);
        }

        // GET: NewProject/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project_Table project_Table = db.Project_Table.Find(id);
            if (project_Table == null)
            {
                return HttpNotFound();
            }
            return View(project_Table);
        }

        // POST: NewProject/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Project_Table project_Table = db.Project_Table.Find(id);
            db.Project_Table.Remove(project_Table);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Dashboard()
        {
            return View();
        }
    }
}
