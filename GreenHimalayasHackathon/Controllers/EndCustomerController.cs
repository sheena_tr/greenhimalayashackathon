﻿using GreenHimalayasHackathon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace GreenHimalayasHackathon.Controllers
{
    public class EndCustomerController : Controller
    {
        private GreenHimalayas_WebEntities db = new GreenHimalayas_WebEntities();

        // GET: EndCustomer
        public ActionResult Index()
        {
            return View("~/Views/EndCustomer/Dashboard.cshtml");
        }

        public ActionResult DigitalForest()
        {
            return View("~/Views/EndCustomer/DigitalForest.cshtml");
        }

        public ActionResult Trees()
        {
            return View("~/Views/EndCustomer/TreesList.cshtml");
        }


        //[Route("GetSatelliteViewForTree/{treeCode}")]
        public ActionResult GetSatelliteViewForTree(int? id)
        {
            TreeViewModel tree = new TreeViewModel();
            try
            {
                int treeId = Convert.ToInt32(id);
                tree = (from t in db.Tree_Master_Table
                                            join p in db.Project_Table on t.Project_id equals p.Project_id
                                            join n in db.Ngo_Table on p.Ngo_id equals n.Ngo_id
                                            where t.Tree_id == treeId
                                            select new TreeViewModel()
                                            {
                                                Carbon_sequetered_per_year = t.Carbon_sequetered_per_year,
                                                Plant_type = t.Plant_type,
                                                NgoName = n.Name,
                                                strLat = t.Latitude,
                                                strLong = t.Longitude,
                                                Plantation_date = t.Plantation_date
                                            }).FirstOrDefault();
                    
                    //db.Tree_Master_Table.Where(x => x.Tree_id == treeId).FirstOrDefault();
                tree.Latitude = Convert.ToDecimal(tree.strLat);
                tree.Longitude = Convert.ToDecimal(tree.strLong);
                tree.Age = Convert.ToString(((DateTime.Now.Subtract(Convert.ToDateTime(tree.Plantation_date)).Days) / 365)) + " yrs " + Convert.ToString(((DateTime.Now.Subtract(Convert.ToDateTime(tree.Plantation_date)).Days) % 365) / 31) + " months";
                  // Convert.ToString( DateTime.Now.Subtract(Convert.ToDateTime(tree.Plantation_date)).Days);
                    }
            catch(Exception ex)
            {

            }
            return View("~/Views/EndCustomer/SatelliteView.cshtml", tree);
        }
    }
}