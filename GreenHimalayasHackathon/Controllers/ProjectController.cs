﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GreenHimalayasHackathon;

namespace GreenHimalayasHackathon.Controllers
{
    public class ProjectController : Controller
    {
        private GreenHimalayas_WebEntities db = new GreenHimalayas_WebEntities();

        // GET: Project
        public ActionResult Index()
        {
            return View(db.Project_Table.ToList());
        }

        // GET: Project/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project_Table project_Table = db.Project_Table.Find(id);
            if (project_Table == null)
            {
                return HttpNotFound();
            }
            return View(project_Table);
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Project/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Project_id,Project_location,No_of_Trees,Inventory,Project_type,Created_Date,Ngo_id")] Project_Table project_Table)
        {
            if (ModelState.IsValid)
            {
                db.Project_Table.Add(project_Table);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(project_Table);
        }

        // GET: Project/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project_Table project_Table = db.Project_Table.Find(id);
            if (project_Table == null)
            {
                return HttpNotFound();
            }
            return View(project_Table);
        }

        // POST: Project/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Project_id,Project_location,No_of_Trees,Inventory,Project_type,Created_Date,Ngo_id")] Project_Table project_Table)
        {
            if (ModelState.IsValid)
            {
                db.Entry(project_Table).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(project_Table);
        }

        // GET: Project/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project_Table project_Table = db.Project_Table.Find(id);
            if (project_Table == null)
            {
                return HttpNotFound();
            }
            return View(project_Table);
        }

        // POST: Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Project_Table project_Table = db.Project_Table.Find(id);
            db.Project_Table.Remove(project_Table);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
