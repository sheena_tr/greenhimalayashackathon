﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GreenHimalayasHackathon;

namespace GreenHimalayasHackathon.Controllers
{
    public class LoginController : Controller
    {
        private GreenHimalayas_WebEntities db = new GreenHimalayas_WebEntities();

        // GET: Login
        public ActionResult Index()
        {
            return View(db.Ngo_Table.ToList());
           // return View();
        }

        // GET: Login/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ngo_Table ngo_Table = db.Ngo_Table.Find(id);
            if (ngo_Table == null)
            {
                return HttpNotFound();
            }
            return View(ngo_Table);
        }

        // GET: Login/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Login/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ngo_id,Name,Email_Id,Mobile_No,Password,Created_Date")] Ngo_Table ngo_Table)
        {
            if (ModelState.IsValid)
            {
                db.Ngo_Table.Add(ngo_Table);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ngo_Table);
        }

        // GET: Login/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ngo_Table ngo_Table = db.Ngo_Table.Find(id);
            if (ngo_Table == null)
            {
                return HttpNotFound();
            }
            return View(ngo_Table);
        }

        // POST: Login/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ngo_id,Name,Email_Id,Mobile_No,Password,Created_Date")] Ngo_Table ngo_Table)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ngo_Table).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ngo_Table);
        }

        // GET: Login/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ngo_Table ngo_Table = db.Ngo_Table.Find(id);
            if (ngo_Table == null)
            {
                return HttpNotFound();
            }
            return View(ngo_Table);
        }

        // POST: Login/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ngo_Table ngo_Table = db.Ngo_Table.Find(id);
            db.Ngo_Table.Remove(ngo_Table);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Ngo_Table login)
        {
            //Ngo_Table project_Table = db.Ngo_Table.Find(login.Email_Id);
            var obj = db.Ngo_Table.Where(a => a.Email_Id.Equals(login.Email_Id) && a.Password.Equals(login.Password)).FirstOrDefault();
            if (obj != null)
            {
                Session["NgoID"] = obj.Ngo_id.ToString();
                Session["NgoName"] = obj.Name.ToString();
                return RedirectToAction("Index", "NewProject");
            }
            else
            {
                return View();
            }
        }
    }
}
