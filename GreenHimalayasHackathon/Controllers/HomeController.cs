﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GreenHimalayasHackathon.Controllers
{
    public class HomeController : Controller
    {
        private GreenHimalayas_WebEntities db = new GreenHimalayas_WebEntities();
        public ActionResult Index()
        {
           
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Ngo_Table login)
        {
            //Ngo_Table project_Table = db.Ngo_Table.Find(login.Email_Id);
            var obj = db.Ngo_Table.Where(a => a.Email_Id.Equals(login.Email_Id) && a.Password.Equals(login.Password)).FirstOrDefault();
            if(obj != null)
            {
                Session["NgoID"] = obj.Ngo_id.ToString();
                Session["NgoName"] = obj.Name.ToString();
                return RedirectToAction("Index", "NewProject");
            }
            else
            {
                return View();
            }
        }
    }
}