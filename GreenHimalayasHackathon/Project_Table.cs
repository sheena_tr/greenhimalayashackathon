//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GreenHimalayasHackathon
{
    using System;
    using System.Collections.Generic;
    
    public partial class Project_Table
    {
        public int Project_id { get; set; }
        public string Project_location { get; set; }
        public Nullable<int> No_of_Trees { get; set; }
        public string Inventory { get; set; }
        public string Project_type { get; set; }
        public Nullable<int> Ngo_id { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public string Project_Name { get; set; }
    }
}
