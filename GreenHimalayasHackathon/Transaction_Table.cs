//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GreenHimalayasHackathon
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transaction_Table
    {
        public int Transaction_id { get; set; }
        public int User_id { get; set; }
        public Nullable<decimal> amount { get; set; }
        public string Platform { get; set; }
        public Nullable<decimal> Co2_emission { get; set; }
    }
}
