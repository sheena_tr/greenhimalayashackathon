//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GreenHimalayasHackathon
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tree_Master_Table
    {
        public int Tree_id { get; set; }
        public string Carbon_sequetered_per_year { get; set; }
        public string Age { get; set; }
        public Nullable<System.DateTime> Plantation_date { get; set; }
        public Nullable<int> Project_id { get; set; }
        public string Plant_type { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Tree_code { get; set; }
    }
}
